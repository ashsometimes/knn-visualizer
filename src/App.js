import React, {Component } from 'react';
import ReactDOM from 'react-dom'
import { Navbar, Image, Button, Nav, DropdownButton, Dropdown, InputGroup, FormControl, Modal } from 'react-bootstrap'
import './App.css'
import logo from './logo.svg'
import image from './image.PNG'

class App extends Component {

  constructor(props) {
    super(props)

    this.state = {
        color: null,
        isTraining: false,
        isPredicting: false,
        input: [],
        k: 5,
        n: 3, 
        outputColor: null,
        colorCodes: [],
        modalShow: false
    }
  }

    // knn helpers
    euclidieanDistance (P1, P2) {
      return Math.sqrt(Math.pow(P1.X - P2.X, 2) + Math.pow(P1.Y - P2.Y, 2))
    }
  
  getNeighbors(posx, posy) {
    var distances = []
    for (let input of this.state.input) {
      var elem = {neighbor: input, distance: this.euclidieanDistance(input, {X: posx, Y: posy})}
      distances = [...distances, elem]
      distances.sort(function(a, b){return a.distance - b.distance});
    }
    return distances
  }

  knn (posx, posy) {
    var neighbors = this.getNeighbors(posx, posy)
    var outputs = []
    for(let i in this.state.colorCodes) {
      var output = {color: this.state.colorCodes[i], n: 0}
      outputs = [...outputs, output]
    }
    var nearestNeighbors = neighbors.slice(0, this.state.k)
    for (let i in nearestNeighbors) {
      outputs[nearestNeighbors[i].neighbor.label].n += 1
    }
    outputs.sort(function(first, second) {
      return second.n - first.n;
    })
    console.log(outputs)
    return outputs[0].color
  }
 
  draw = async (event) => {
    var canvas = document.getElementById("canvas")
    var context = canvas.getContext("2d");
    var pos = this.getMousePos(canvas, event)
    var posx = pos.x
    var posy = pos.y
    if (this.state.isTraining && this.state.color !== null) {
    context.fillStyle = this.state.color
    context.beginPath()
    context.arc(posx, posy, 9, 0, 2*Math.PI)
    this.setState({input: [...this.state.input, {X: posx, Y: posy, label: this.state.colorCodes.indexOf(this.state.color)}]})
    context.fill()
  }
  else if (this.state.isPredicting) {
    var color = await this.knn(posx, posy)
    console.log(color)
    document.onmousemove = this.animateCircles(color)
    context.fillStyle = color
    context.beginPath()
    context.arc(posx, posy, 9, 0, 2*Math.PI)
    context.fill()
    this.setState({outputColor: null})
    }
  }



  animateCircles = (givenColor) => (event) => {
    let circle = document.createElement("div");
    circle.setAttribute("class", "circle");
    document.body.appendChild(circle);

    circle.style.left = event.clientX + 'px';
    circle.style.top = event.clientY + 'px';

    var color = givenColor
    circle.style.borderColor = color;

    circle.style.transition = "all 0.5s linear 0s";

    circle.style.left = circle.offsetLeft - 20 + 'px';
    circle.style.top = circle.offsetTop - 20 + 'px';

    circle.style.width = "50px";
    circle.style.height = "50px";
    circle.style.borderWidth = "5px";
    circle.style.opacity = 0;
    
    setTimeout(function() {
              circle.remove();
          }, 500);
  }


  
  componentDidMount() {
    this.reset()

  }

  getMousePos(canvas, event) {
    var rect = canvas.getBoundingClientRect();
    return {
      x: event.clientX - rect.left,
      y: event.clientY - rect.top
    }
  }

  handleChange = (name) => (event) => {
    this.setState({[name]: parseInt(event.target.value)})
  }
  reset() {
    const canvas = ReactDOM.findDOMNode(this.refs.canvas)
    const ctx = canvas.getContext("2d");
    ctx.fillStyle = 'rgb(255,255,255)';
    ctx.fillRect(0, 0, window.innerWidth - 40, window.innerHeight - 75)
    this.setState({isTraining: false, isPredicting: false, color: null, input: [], k: 5, n:3})    
  }


  handleColor = (color) => (event) => {
    this.setState({color})
  }

  startTraining () {
    this.setState({isTraining: true})
    var colorCodes = []
    for (let i = 0; i < this.state.n; i++) {
      var ColorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')'
      colorCodes = [...colorCodes, ColorCode]
    }
    this.setState({colorCodes: colorCodes})
  }

  mouseMovement (event) {
    var color = this.knn(event.screenX, event.screenY)
    document.onmousemove = this.animateCircles(color)
    this.setState({isPredicting: true})
    }



  render(){
    return (
      <>
        <Navbar bg="light" expand="lg">
            <Navbar.Brand href="/">Click on right</Navbar.Brand>
              <Navbar.Toggle aria-controls="basic-navbar-nav" />
              <Navbar.Collapse id="basic-navbar-nav">
              <img src={logo} className="App-logo" alt="logo" style={{"pointer-events": "all"}} onClick={() => {this.setState({modalShow: true})}}></img>
              <Nav className="mr-auto" >
                <InputGroup className="w-50 mb-3">
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">k</InputGroup.Text>
                  </InputGroup.Prepend>
                  <FormControl
                    disabled={this.state.isTraining || this.state.isPredicting}
                    placeholder="no of nearest nbrs"
                    aria-label="kValue"
                    aria-describedby="basic-addon1"
                    onChange={this.handleChange("k")}
                  />
                  <InputGroup.Prepend>
                    <InputGroup.Text id="basic-addon1">n</InputGroup.Text>
                  </InputGroup.Prepend>

                  <FormControl
                    disabled={this.state.isTraining || this.state.isPredicting}
                    placeholder="no of classes"
                    aria-label="noOfClasses"
                    aria-describedby="basic-addon1"
                    onChange={this.handleChange("n")}
                  />
                </InputGroup>
                <div className="divider"/>
                {this.state.isTraining?<>
                  <DropdownButton variant="outline-dark" id="dropdown-basic-button" title="Choose Class">
                    {
                      this.state.colorCodes.map((value, key) => (
                        <Dropdown.Item onSelect={this.handleColor(value)} key={key}>
                          Class {JSON.stringify(key)}
                        </Dropdown.Item>
                      )
                    )}
                  </DropdownButton>
                </>:<></>}
                <div className="divider"/>
                <Nav.Link disabled={this.state.isPredicting} onClick = {() => {this.startTraining()}} 
                className="button" >Start Training</Nav.Link>
                <Nav.Link disabled={this.state.isPredicting} onClick = {() => {this.setState({isTraining: false})}} className="button" >Stop Training</Nav.Link>
                <Nav.Link className="button" onClick = {() => {this.setState({isPredicting: true})}}>Predict</Nav.Link>
            </Nav>
            <Button variant="outline-dark" onClick = {() => this.reset()}>Reset</Button>
          </Navbar.Collapse>              
        </Navbar>
        {this.state.modalShow ? 
        <>
          <Modal 
          show={()=>{this.setState({modalShow: true})}} 
          onHide={()=>{this.setState({modalShow: true})}}         
          backdrop="static"
          keyboard={false}>
            <Modal.Header>
            <Modal.Title>Tutorial for the Visualizer</Modal.Title>
            <button onClick={()=>{this.setState({modalShow: false})}} type="button" class="close"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
          </Modal.Header>

          <Modal.Body>
            <ul>
              <li>Set the value on k and n</li>
              <li>Click on start training</li>
              <li>Choose appropriate and start drawing points on the canvas</li>
              <li>After you are finished with the dataset, click on stop training</li>
              <li>Click on Predict and draw the query points to visualize the KNN algorithm</li>
            </ul>
          </Modal.Body>
            <Image src={image} fluid />
          <Modal.Footer>
          </Modal.Footer>
        </Modal></>:<></>}

        <canvas id="canvas" ref="canvas"
              width={window.innerWidth}
              height={window.innerHeight - 50}
              onMouseMove={()=> {this.mouseMovement.bind(this)}}
              onClick={
                  e => {
                      let nativeEvent = e.nativeEvent;
                      this.draw(nativeEvent);
                  }}
      />
    </>);
  }

}

export default App;
